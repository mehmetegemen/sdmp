const AWS = require("aws-sdk");

AWS.config.update({
  region: "us-east-1",
  endpoint: process.env.DYNAMODB_ENDPOINT || "http://localhost:8000"
});

const docClient = new AWS.DynamoDB.DocumentClient();

module.exports = docClient;
