const docClient = require("./connectDB-dynamodb");

function removeUser(userId) {
  const params = {
    TableName: "users",
    Key: {
      user_id: userId
    }
  };
  return new Promise((resolve, reject) => {
    docClient.delete(params, (err, doc) => {
      if (err) return reject(err);
      resolve(doc);
    });
  });
}

module.exports = removeUser;
