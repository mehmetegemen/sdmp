const dotenv = require("dotenv");
dotenv.config();
const mongoose = require("mongoose");

// Old code is commented out

// Build the connection string

const dbURI = "mongodb://localhost:27017/sdmp";

let db;

function connectDB() {
  return new Promise(async (resolve, reject) => {
    if (db) return resolve(db);
    await mongoose.connect(
      dbURI,
      { useNewUrlParser: true }
    );

    db = mongoose.connection;
    resolve(mongoose.connection);
  });
}

module.exports = connectDB;
