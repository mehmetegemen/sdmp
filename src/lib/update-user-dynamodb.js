const docClient = require("../lib/connectDB-dynamodb");

function updateUser(data, userId) {
  let updateExpression = "set",
    expressionAttributeValues = {},
    expressionAttributeNames = {};
  let i = 0;
  for (const key of Object.keys(data)) {
    expressionAttributeValues[":" + key + "Val"] = data[key];
    expressionAttributeNames["#" + key] = key;
    if (i === Object.keys(data).length - 1) {
      updateExpression += " #" + key + "=:" + key + "Val";
    } else {
      updateExpression += " #" + key + "=:" + key + "Val,";
      i++;
    }
  }
  const params = {
    TableName: "users",
    Key: {
      user_id: userId
    },
    UpdateExpression: updateExpression,
    ExpressionAttributeValues: expressionAttributeValues,
    ExpressionAttributeNames: expressionAttributeNames,
    ReturnValues: "UPDATED_NEW"
  };

  return new Promise((resolve, reject) => {
    docClient.update(params, (err, doc) => {
      if (err) return reject(err);
      resolve(doc);
    });
  });
}

module.exports = updateUser;
