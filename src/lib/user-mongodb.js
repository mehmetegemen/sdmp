const mongoose = require("mongoose");
const autoIncrement = require("mongoose-auto-increment");

autoIncrement.initialize(mongoose.connection);

delete mongoose.connection.models["User"];

const UserSchema = new mongoose.Schema({
  name: { type: String, required: true },
  email: { type: String, required: true },
  mobile_number: { type: Number },
  address: { type: String, default: "" },
  company: { type: String, default: "" },
  title: { type: String, default: "" },
  interest: { type: Array, default: [] }
});

UserSchema.plugin(autoIncrement.plugin, { model: "User", field: "user_id" });

module.exports = mongoose.model("User", UserSchema);
