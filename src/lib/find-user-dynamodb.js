const docClient = require("./connectDB-dynamodb");

function findUser(data) {
  // key E.g. mobile_number
  const key = data.key;
  // value E.g 8855501354
  const value = data.value;
  let keyConditionExpression,
    expressionAttributeValues = {},
    valPlaceholder;
  if (key === "email") {
    valPlaceholder = ":" + key + "Val";
    keyConditionExpression = `email = ${valPlaceholder}`;
    expressionAttributeValues[valPlaceholder] = value;
  } else if (key === "mobile_number") {
    valPlaceholder = ":" + key + "Val";
    keyConditionExpression = `mobile_number = ${valPlaceholder}`;
    expressionAttributeValues[valPlaceholder] = value;
  } else if (key === "user_id") {
    valPlaceholder = ":" + key + "Val";
    keyConditionExpression = `user_id = ${valPlaceholder}`;
    expressionAttributeValues[valPlaceholder] = value;
  } else {
    return new Error("Cannot find user, invalid query key.");
  }
  const params = {
    TableName: "users",
    IndexName: key,
    KeyConditionExpression: keyConditionExpression,
    ExpressionAttributeValues: expressionAttributeValues
  };
  return new Promise((resolve, reject) => {
    docClient.query(params, function(err, doc) {
      if (err) {
        return reject(err);
      } else {
        resolve(doc);
      }
    });
  });
}

module.exports = findUser;
