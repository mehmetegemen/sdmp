"use strict";
const updateProfilePolicy = require("../policies/update-profile-policy");
const sanitizeUser = require("../utils/sanitize-user");
const findUser = require("../lib/find-user-dynamodb");
const updateUser = require("../lib/update-user-dynamodb");

/**
 * @api {post} /update-interest Update Profile [POST]
 * @apiGroup Profile
 * @apiDescription This api is used to update ineterst of the user.
 *
 * @apiParam (Query) {String} id email or id of the user.
 *
 * @apiParam {String} name name of the user.
 * @apiParam {String} email email id of the user.
 * @apiParam {String} mobile_address Mobile Address of the user.
 * @apiParam {String} address address of the user.
 * @apiParam {String} company company of the user.
 * @apiParam {String} title title of the user.
 * @apiParam {String} interest interest of the user.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "address": "<address>",
 *       "company": "<company>",
 *       "title": "<title>",
 *       "interest": [<updated interest>],
 *       "name": "<name>",
 *       "email": "<email>",
 *       "mobile_number": "8004913901",
 *       "user_id": 34
 *     }
 *
 * @apiErrorExample Error-Response 500:
 *     HTTP/1.1 500 Error on server side.
 *     {
 *        "Could not update the user."
 *     }
 * @apiErrorExample Error-Response 422:
 *     HTTP/1.1 422 Error on server side.
 *     {
 *        "Could not process request body."
 *     }
 * @apiErrorExample Error Response 422:
 *     HTTP/1.1 422 Error on serverside.
 *     {
 *        "Malformed id."
 *     }
 * @apiErrorExample Error-Response 404:
 *     HTTP/1.1 404 Error Could not find the user.
 *     {
 *        "User not found."
 *     }
 */

module.exports = async (event, context) => {
  try {
    context.callbackWaitsForEmptyEventLoop = false;
    console.log("event: ", event.queryStringParameters);
    const query = event.queryStringParameters;
    // Check if query string of id is valid
    // Look for chars in user ids and emails
    if (!/^[a-zA-Z0-9@_\-.]+$/.test(query.id)) {
      return {
        statusCode: 422,
        headers: { "Content-Type": "text/plain" },
        body: "Malformed id."
      };
    }
    // Convert query id to Number type if
    // query id consists of only numbers.
    // Otherwise it is string
    if (/^[\d]+$/.test(query.id)) query.id = Number(query.id);
    const body = JSON.parse(event.body);
    const error = updateProfilePolicy(body);
    // Check if there is any problem with req body
    // "error" is null if there isn't any
    if (error) {
      // There is error
      // Exit with failure
      return {
        statusCode: 422,
        headers: { "Content-Type": "text/plain" },
        body:
          "Could not process request body. " + error.details[0].message + "."
      };
    }
    // Define the type of the query.id
    // Email(String) or mobile_number(Number)
    // Check if users with same email exists
    let criteria = {
      key: "email",
      value: query.id
    };
    // Example return value of findUser():
    // { Items: [], Count: 0, ScannedCount: 0 }
    let userResponse = null;
    if (typeof query.id === "string") userResponse = await findUser(criteria);
    // Check if user with given email is found
    // and assign to a variable. Otherwise next
    // block will stop the code
    let found = true;
    if (!userResponse) {
      // User with same email or mobile number exists
      // Exit with failure
      /*return {
        statusCode: 404,
        headers: { "Content-Type": "text/plain" },
        body: "User not found."
      };*/
      found = false;
    } else if (userResponse.Count === 0) {
      // Id is not email
      found = false;
    }
    // Check if users with same email exists
    criteria = {
      key: "mobile_number",
      value: query.id
    };
    if (typeof query.id === "number") userResponse = await findUser(criteria);
    // All query types are checked
    // Return error if still type doesn't match
    if (!userResponse) {
      return {
        statusCode: 422,
        headers: { "Content-Type": "text/plain" },
        body: "Malformed id."
      };
    }
    if (userResponse.Count === 0 && !found) {
      // User with same email or mobile number exists
      // Exit with failure
      return {
        statusCode: 404,
        headers: { "Content-Type": "text/plain" },
        body: "User not found."
      };
    }
    // User exists
    // find and update user with given id
    userResponse = await updateUser(body, userResponse.Items[0].user_id);
    // User response looks like
    // { Attributes: {
    //  "email": "test@email.com"
    //  ... } }
    userResponse = userResponse.Attributes;
    // Sanitize user
    userResponse = sanitizeUser(userResponse);
    return {
      statusCode: 200,
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(userResponse)
    };
  } catch (err) {
    console.error(err);
    return {
      statusCode: 500,
      headers: { "Content-Type": "text/plain" },
      body: "Could not update the user."
    };
  }
};
