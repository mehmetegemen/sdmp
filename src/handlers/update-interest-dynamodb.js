"use strict";
const sanitizeUser = require("../utils/sanitize-user");
const updateInterestPolicy = require("../policies/update-interest-policy");
const updateUser = require("../lib/update-user-dynamodb");
const findUser = require("../lib/find-user-dynamodb");

/**
 * @api {put} /update-interest Update Interest [PUT]
 * @apiGroup Profile
 * @apiDescription This api is used to update interest of the user.
 *
 * @apiParam (Query) {String} id email or id of the user.
 *
 * @apiParam (body) {String} interest interest of the user.
 * @apiParam (body) {Boolean} remove true if you want to delete the above
 *                                   interest and false if you want to add that
 *                                   interest to the user profile.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "address": "<address>",
 *       "company": "<company>",
 *       "title": "<title>",
 *       "interest": [],
 *       "name": "<name>",
 *       "email": "<email>",
 *       "mobile_number": "8004913901",
 *       "user_id": 34,
 *     }
 *
 * @apiErrorExample Error-Response 500:
 *     HTTP/1.1 500 Error on server side.
 *     {
 *        "Could not update interets."
 *     }
 * @apiErrorExample Error-Response 404:
 *     HTTP/1.1 404 Error Could not find the user.
 *     {
 *        "User not found."
 *     }
 * @apiErrorExample Error Response 422:
 *     HTTP/1.1 422 Error on serverside.
 *     {
 *        "Malformed id."
 *     }
 */

module.exports = async (event, context) => {
  try {
    // I don't want to wait for an infinitely
    // long lasting request, it was false before
    context.callbackWaitsForEmptyEventLoop = true;
    console.log("event: ", event.queryStringParameters);
    const query = event.queryStringParameters;
    // Check if query string of id is valid
    // Look for chars in user ids and emails
    if (!/^[a-zA-Z0-9@_\-.]+$/.test(query.id)) {
      return {
        statusCode: 422,
        headers: { "Content-Type": "text/plain" },
        body: "Malformed id."
      };
    }
    // Convert query id to Number type if
    // query id consists of only numbers.
    // Otherwise it is string
    if (/^[\d]+$/.test(query.id)) query.id = Number(query.id);
    const body = JSON.parse(event.body);
    const error = updateInterestPolicy(body);
    // Check if there is any problem with req body
    // "error" is null if there isn't any
    if (error) {
      // There is error
      // Exit with failure
      return {
        statusCode: 422,
        headers: { "Content-Type": "text/plain" },
        body:
          "Could not process request body. " + error.details[0].message + "."
      };
    }
    // Define the type of the query.id
    // Email(String) or mobile_number(Number)
    // Check if users with same email exists
    let criteria = {
      key: "email",
      value: query.id
    };
    // Example return value of findUser():
    // { Items: [], Count: 0, ScannedCount: 0 }
    let userResponse = null;
    if (typeof query.id === "string") userResponse = await findUser(criteria);
    // Check if user with given email is found
    // and assign to a variable. Otherwise next
    // block will stop the code
    let found = true;
    if (!userResponse) {
      // User with same email or mobile number exists
      // Exit with failure
      /*return {
        statusCode: 404,
        headers: { "Content-Type": "text/plain" },
        body: "User not found."
      };*/
      found = false;
    } else if (userResponse.Count === 0) {
      // Id is not email
      found = false;
    }
    // Check if users with same email exists
    criteria = {
      key: "mobile_number",
      value: query.id
    };
    if (typeof query.id === "number") userResponse = await findUser(criteria);
    // All query types are checked
    // Return error if still type doesn't match
    if (!userResponse) {
      return {
        statusCode: 422,
        headers: { "Content-Type": "text/plain" },
        body: "Malformed id."
      };
    }
    if (userResponse.Count === 0 && !found) {
      // User with same email or mobile number exists
      // Exit with failure
      return {
        statusCode: 404,
        headers: { "Content-Type": "text/plain" },
        body: "User not found."
      };
    }
    // User exists
    // Get interests
    let interest = userResponse.Items[0].interest || [];
    // Lower case all interests of request body
    for (const index of body.interest.keys()) {
      body.interest[index] = body.interest[index].toLowerCase();
    }
    if (body.remove) {
      interest = interest.filter(
        elem => body.interest.indexOf(elem.toLowerCase()) < 0
      );
    } else {
      interest = interest.filter(
        elem => body.interest.indexOf(elem.toLowerCase()) < 0
      );
      interest.push(...body.interest);
    }
    const reqObj = { interest };
    // find and update user with given id
    await updateUser(reqObj, userResponse.Items[0].user_id);
    if (typeof query.id === "number") {
      criteria = {
        key: "mobile_number",
        value: query.id
      };
    } else {
      criteria = {
        key: "email",
        value: query.id
      };
    }
    userResponse = await findUser(criteria);
    // Sanitize user
    userResponse = sanitizeUser(userResponse.Items[0]);
    return {
      statusCode: 200,
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(userResponse)
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers: { "Content-Type": "text/plain" },
      body: "Could not update interets."
    };
  }
};
