"use strict";
const docClient = require("../lib/connectDB-dynamodb");
const sanitizeUser = require("../utils/sanitize-user");
const createProfilePolicy = require("../policies/create-profile-policy");
const findUser = require("../lib/find-user-dynamodb");

/**
 * @api {post} /create-profile Create Profile [POST]
 * @apiGroup Profile
 * @apiDescription This api is used to create a profile.
 * @apiParam {String} name name of the user.
 * @apiParam {String} email email id of the user.
 * @apiParam {String} mobile_number Mobile Number of the user.
 * @apiParam {String} address address of the user.
 * @apiParam {String} company company of the user.
 * @apiParam {String} title title of the user.
 * @apiParam {String} interest interest of the user.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *       "address": "<address>",
 *       "company": "<company>",
 *       "title": "<title>",
 *       "interest": [],
 *       "name": "<name>",
 *       "email": "<email>",
 *       "mobile_number": "8004913901",
 *       "user_id": 34,
 *     }
 *
 * @apiErrorExample Error-Response 403:
 *     HTTP/1.1 403 Unable to signup.
 *     {
 *       <message>
 *     }
 * @apiErrorExample Error-Response 500:
 *     HTTP/1.1 500 Error on server side.
 *     {
 *        "Could not create the user."
 *     }
 * @apiErrorExample Error-Response 409:
 *     HTTP/1.1 409 User with given credentials exists.
 *     {
 *        "User with same credentials exists."
 *     }
 */

function getUserIdCount() {
  const params = {
    TableName: "identitycounter",
    Key: {
      field: "user_id"
    }
  };
  return new Promise((resolve, reject) => {
    docClient.get(params, (err, doc) => {
      if (err) return reject(err);
      // Example "doc" value:
      // { Item: { idcounter: 0, field: 'user_id' } }
      resolve(doc.Item.idcounter);
    });
  });
}

async function incrementUserIdCount() {
  const userIdCount = await getUserIdCount();
  const params = {
    TableName: "identitycounter",
    Key: {
      field: "user_id"
    },
    UpdateExpression: "set idcounter = :c",
    ExpressionAttributeValues: {
      ":c": userIdCount + 1
    }
  };
  return new Promise((resolve, reject) => {
    docClient.update(params, (err, doc) => {
      if (err) return reject(err);
      resolve(doc);
    });
  });
}

async function createUser(data) {
  try {
    // Get latest user id
    const userIdCount = await getUserIdCount();
    return new Promise((resolve, reject) => {
      docClient.put(
        {
          TableName: "users",
          Item: {
            user_id: userIdCount,
            name: data.name,
            email: data.email,
            mobile_number: data.mobile_number,
            address: data.address,
            company: data.company,
            title: data.title,
            interest: data.interest
          },
          ReturnValues: "ALL_OLD"
        },
        async (err, doc) => {
          if (err) return reject(err);
          await incrementUserIdCount();
          resolve(doc);
        }
      );
    });
  } catch (err) {
    console.log(err.stack);
  }
}

module.exports = async (event, context) => {
  try {
    // I don't want to wait for an infinitely
    // long lasting request, it was false before
    context.callbackWaitsForEmptyEventLoop = true;
    console.log("event: ", event);
    const body = JSON.parse(event.body);
    const error = createProfilePolicy(body);
    // Check if there is any problem with req body
    // "error" is null if there isn't any
    if (error) {
      // There is error
      // Exit with failure
      return {
        statusCode: 422,
        headers: { "Content-Type": "text/plain" },
        body:
          "Could not process request body. " + error.details[0].message + "."
      };
    }
    // Check if users with same email exists
    let criteria = {
      key: "email",
      value: body.email
    };
    // Example return value of findUser():
    // { Items: [], Count: 0, ScannedCount: 0 }
    let userResponse = await findUser(criteria);
    if (userResponse.Count > 0) {
      // User with same email or mobile number exists
      // Exit with failure
      return {
        statusCode: 409,
        headers: { "Content-Type": "text/plain" },
        body: "User with same credentials exists."
      };
    }
    // Check if users with same email exists
    criteria = {
      key: "mobile_number",
      value: body.mobile_number
    };
    userResponse = await findUser(criteria);
    if (userResponse.Count > 0) {
      // User with same email or mobile number exists
      // Exit with failure
      return {
        statusCode: 409,
        headers: { "Content-Type": "text/plain" },
        body: "User with same credentials exists."
      };
    }
    // Create user
    await createUser(body);
    criteria = {
      key: "email",
      value: body.email
    };
    userResponse = await findUser(criteria);
    // Sanitize user
    userResponse = sanitizeUser(userResponse.Items[0]);
    const response = {
      statusCode: 201,
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(userResponse)
    };
    return response;
  } catch (err) {
    console.log(err);
    // Error while creating user record
    // Send 500 response via serverless
    return {
      statusCode: 500,
      headers: { "Content-Type": "text/plain" },
      body: "Could not create the user."
    };
  }
};
