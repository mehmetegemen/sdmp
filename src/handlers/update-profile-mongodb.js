"use strict";
//const connectToDatabase = require("../lib/connectDB-mongodb");
const User = require("../lib/user-mongodb");
const updateProfilePolicy = require("../policies/update-profile-policy");
const sanitizeUser = require("../utils/sanitize-user");
const connectToDatabase = require("../lib/connectDB-mongodb");

/**
 * @api {post} /update-interest Update Profile [POST]
 * @apiGroup Profile
 * @apiDescription This api is used to update ineterst of the user.
 *
 * @apiParam (Query) {String} id email or id of the user.
 *
 * @apiParam {String} name name of the user.
 * @apiParam {String} email email id of the user.
 * @apiParam {String} mobile_address Mobile Address of the user.
 * @apiParam {String} address address of the user.
 * @apiParam {String} company company of the user.
 * @apiParam {String} title title of the user.
 * @apiParam {String} interest interest of the user.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "address": "<address>",
 *       "company": "<company>",
 *       "title": "<title>",
 *       "interest": [<updated interest>],
 *       "name": "<name>",
 *       "email": "<email>",
 *       "mobile_number": "8004913901",
 *       "user_id": 34
 *     }
 *
 * @apiErrorExample Error-Response 500:
 *     HTTP/1.1 500 Error on server side.
 *     {
 *        "Could not update the user."
 *     }
 * @apiErrorExample Error-Response 422:
 *     HTTP/1.1 422 Error on server side.
 *     {
 *        "Could not process request body."
 *     }
 */

function updateUser(data, criteria) {
  return new Promise((resolve, reject) => {
    User.findOneAndUpdate(
      criteria,
      { $set: data },
      { new: true },
      (err, doc) => {
        if (err) return reject(err);
        resolve(doc);
    });
  });
}

module.exports = async (event, context) => {
  try {
    context.callbackWaitsForEmptyEventLoop = false;
    console.log("event: ", event.queryStringParameters);
    const query = event.queryStringParameters;
    const body = JSON.parse(event.body);
    const error = updateProfilePolicy(body);
    // Check if there is any problem with req body
    // "error" is null if there isn't any
    if (error) {
      // There is error
      // Exit with failure
      return {
        statusCode: 422,
        headers: { "Content-Type": "text/plain" },
        body:
          "Could not process request body. " + error.details[0].message + "."
      };
    }
    await connectToDatabase();
    const criteria = {};
    if (typeof query.id === "string") criteria.email = query.id;
    else criteria.mobile_number = query.id;
    // Find user by email or mobile number
    let user = await User.findOne(criteria);
    if (!user) {
      // User does not exist
      // Send an error response
      return {
        statusCode: 404,
        headers: { "Content-Type": "text/plain" },
        body: "User not found."
      };
    }
    // User exists
    // find and update user with given id
    user = await updateUser(body);
    // Sanitize user by removing mongodb specific
    // properties from user model object. Otherwise
    // attackers can guess next records of mongodb
    // if they know _id property of an arbitrary
    // record.
    user = sanitizeUser(user);
    return {
      statusCode: 200,
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(user)
    };
  } catch (err) {
    console.error(err);
    return {
      statusCode: 500,
      headers: { "Content-Type": "text/plain" },
      body: "Could not update the user."
    }
  }
};
