"use strict";
//const connectToDatabase = require("../lib/connectDB-mongodb");
const User = require("../lib/user-mongodb");
const sanitizeUser = require("../utils/sanitize-user");
const updateInterestPolicy = require("../policies/update-interest-policy");
const connectToDatabase = require("../lib/connectDB-mongodb");

/**
 * @api {put} /update-interest Update Interest [PUT]
 * @apiGroup Profile
 * @apiDescription This api is used to update interest of the user.
 *
 * @apiParam (Query) {String} id email or id of the user.
 *
 * @apiParam (body) {String} interest interest of the user.
 * @apiParam (body) {Boolean} remove true if you want to delete the above
 *                                   interest and false if you want to add that
 *                                   interest to the user profile.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "address": "<address>",
 *       "company": "<company>",
 *       "title": "<title>",
 *       "interest": [],
 *       "name": "<name>",
 *       "email": "<email>",
 *       "mobile_number": "8004913901",
 *       "user_id": 34,
 *     }
 *
 * @apiErrorExample Error-Response 500:
 *     HTTP/1.1 500 Error on server side.
 *     {
 *        "Could not update interets."
 *     }
 * @apiErrorExample Error-Response 404:
 *     HTTP/1.1 404 Error Could not find the user.
 *     {
 *        "User not found."
 *     }
 */

module.exports = async (event, context) => {
  try {
    // I don't want to wait for an infinitely
    // long lasting request, it was false before
    context.callbackWaitsForEmptyEventLoop = true;
    console.log("event: ", event.queryStringParameters);
    const query = event.queryStringParameters;
    // Check if query string of id is valid
    // Look for chars in user ids and emails
    if (!/[a-zA-Z0-9@_\-.]/g.test(query.id)) {
      return {
        statusCode: 422,
        headers: { "Content-Type": "text/plain" },
        body: "Id format is broken."
      }
    }
    //await connectToDatabase();
    const criteria = {};
    if (typeof query.id === "string") criteria.email = query.id;
    else criteria.mobile_number = query.id;
    // Find user by email or mobile number
    let user = await User.findOne(criteria);
    if (!user) {
      // User does not exist
      // Send an error response
      return {
        statusCode: 404,
        headers: { "Content-Type": "text/plain" },
        body: "User not found."
      };
    }
    // Get and parse request body
    const body = JSON.parse(event.body);
    const error = updateInterestPolicy(body);
    // Check if there is any problem with req body
    // "error" is null if there isn't any
    if (error) {
      // There is error
      // Exit with failure
      return {
        statusCode: 422,
        headers: { "Content-Type": "text/plain" },
        body:
          "Could not process request body. " + error.details[0].message + "."
      };
    }
    console.log("body: ", body);
    await connectToDatabase();
    let interest = user.interest || [];
    // Lower case all interests of request body
    for (const index of body.interest.keys()) {
      body.interest[index] = body.interest[index].toLowerCase();
    }
    if (body.remove) {
      interest = interest.filter(
        elem => body.interest.indexOf(elem.toLowerCase()) < 0
      );
    } else {
      interest = interest.filter(
        elem => body.interest.indexOf(elem.toLowerCase()) < 0
      );
      interest.push(...body.interest);
    }
    user.interest = interest;
    await user.save();
    // Sanitize user by removing mongodb specific
    // properties from user model object. Otherwise
    // attackers can guess next records of mongodb
    // if they know _id property of an arbitrary
    // record.
    user = sanitizeUser(user.toObject());
    return {
      statusCode: 200,
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(user)
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers: { "Content-Type": "text/plain" },
      body: "Could not update interets."
    };
  }
};
