"use strict";
//const connectToDatabase = require("../lib/connectDB-mongodb");
const User = require("../lib/user-mongodb");
const sanitizeUser = require("../utils/sanitize-user");
const createProfilePolicy = require("../policies/create-profile-policy");

/**
 * @api {post} /create-profile Create Profile [POST]
 * @apiGroup Profile
 * @apiDescription This api is used to create a profile.
 * @apiParam {String} name name of the user.
 * @apiParam {String} email email id of the user.
 * @apiParam {String} mobile_number Mobile Number of the user.
 * @apiParam {String} address address of the user.
 * @apiParam {String} company company of the user.
 * @apiParam {String} title title of the user.
 * @apiParam {String} interest interest of the user.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *       "address": "<address>",
 *       "company": "<company>",
 *       "title": "<title>",
 *       "interest": [],
 *       "name": "<name>",
 *       "email": "<email>",
 *       "mobile_number": "8004913901",
 *       "user_id": 34,
 *     }
 *
 * @apiErrorExample Error-Response 403:
 *     HTTP/1.1 403 Unable to signup.
 *     {
 *       <message>
 *     }
 * @apiErrorExample Error-Response 500:
 *     HTTP/1.1 500 Error on server side.
 *     {
 *        "Could not create the user."
 *     }
 * @apiErrorExample Error-Response 409:
 *     HTTP/1.1 409 User with given credentials exists.
 *     {
 *        "User with same credentials exists."
 *     }
 */

function createUser(data) {
  return new Promise((resolve, reject) => {
    User.create(
      {
        name: data.name,
        email: data.email,
        mobile_number: data.mobile_number,
        address: data.address,
        company: data.company,
        title: data.title,
        interest: data.interest
      },
      (err, doc) => {
        if (err) return reject(err);
        //console.log(doc);
        resolve(doc);
      }
    );
  });
}

module.exports = async (event, context) => {
  try {
    // I don't want to wait for an infinitely
    // long lasting request, it was false before
    context.callbackWaitsForEmptyEventLoop = true;
    //await connectToDatabase();
    console.log("event: ", event);
    const body = JSON.parse(event.body);
    const error = createProfilePolicy(body);
    // Check if there is any problem with req body
    // "error" is null if there isn't any
    if (error) {
      // There is error
      // Exit with failure
      return {
        statusCode: 422,
        headers: { "Content-Type": "text/plain" },
        body:
          "Could not process request body. " + error.details[0].message + "."
      };
    }
    const criteria = {
      $or: [
        {
          email: body.email
        },
        {
          mobile_number: body.mobile_number
        }
      ]
    };
    let user = await User.findOne(criteria);
    if (user) {
      // User with same email or mobile number exists
      // Exit with failure
      return {
        statusCode: 409,
        headers: { "Content-Type": "text/plain" },
        body: "User with same credentials exists."
      };
    }
    user = await createUser(body);
    // Sanitize user by removing mongodb specific
    // properties from user model object. Otherwise
    // attackers can guess next records of mongodb
    // if they know _id property of an arbitrary
    // record.
    user = sanitizeUser(user.toObject());
    const response = {
      statusCode: 201,
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(user)
    };
    return response;
  } catch (err) {
    // Error while creating user record
    // Send 500 response via serverless
    return {
      statusCode: 500,
      headers: { "Content-Type": "text/plain" },
      body: "Could not create the user."
    };
  }
};
