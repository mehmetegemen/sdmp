function sanitizeUser(data) {
  // Copy an object's properties to a new
  // one. By doing this we will omit properties
  // which we don't want attackers to see such as
  // _id and _v. Also another use case is
  // omitting password.
  return {
    name: data.name,
    email: data.email,
    mobile_number: data.mobile_number,
    address: data.address,
    company: data.company,
    title: data.title,
    interest: data.interest,
    user_id: data.user_id
  };
}

module.exports = sanitizeUser;
