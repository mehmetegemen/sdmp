const AWS = require("aws-sdk");

const endpoint = process.env.DYNAMODB_ENDPOINT || "http://localhost:8000";

AWS.config.update({
  region: "us-east-1",
  endpoint: endpoint
});

const dynamodb = new AWS.DynamoDB();
const docClient = new AWS.DynamoDB.DocumentClient();

const usersParams = {
  TableName: "users",
  KeySchema: [{ AttributeName: "user_id", KeyType: "HASH" }],
  AttributeDefinitions: [
    { AttributeName: "user_id", AttributeType: "N" },
    { AttributeName: "email", AttributeType: "S" },
    { AttributeName: "mobile_number", AttributeType: "N" }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 1000,
    WriteCapacityUnits: 1000
  },
  GlobalSecondaryIndexes: [
    {
      IndexName: "email",
      KeySchema: [{ AttributeName: "email", KeyType: "HASH" }],
      Projection: { ProjectionType: "ALL" },
      ProvisionedThroughput: {
        ReadCapacityUnits: 1000,
        WriteCapacityUnits: 1000
      }
    },
    {
      IndexName: "mobile_number",
      KeySchema: [{ AttributeName: "mobile_number", KeyType: "HASH" }],
      Projection: { ProjectionType: "ALL" },
      ProvisionedThroughput: {
        ReadCapacityUnits: 1000,
        WriteCapacityUnits: 1000
      }
    }
  ]
};

dynamodb.createTable(usersParams, function(err, data) {
  if (err) {
    console.error(
      "Unable to create users table. Error JSON:",
      JSON.stringify(err, null, 2)
    );
  } else {
    console.log(
      "Created users table. Table description JSON:",
      JSON.stringify(data, null, 2)
    );
  }
});

const counterParams = {
  TableName: "identitycounter",
  KeySchema: [{ AttributeName: "field", KeyType: "HASH" }],
  AttributeDefinitions: [{ AttributeName: "field", AttributeType: "S" }],
  ProvisionedThroughput: {
    ReadCapacityUnits: 1000,
    WriteCapacityUnits: 1000
  }
};

const counterDocParams = {
  TableName: "identitycounter",
  Item: {
    idcounter: 0,
    field: "user_id"
  }
};

dynamodb.createTable(counterParams, function(err, data) {
  if (err) {
    console.error(
      "Unable to create identitycounter table. Error JSON:",
      JSON.stringify(err, null, 2)
    );
  } else {
    console.log(
      "Created identitycounter table. Table description JSON:",
      JSON.stringify(data, null, 2)
    );
    docClient.put(counterDocParams, function(err, data) {
      if (err) return console.error(err);
      console.log(
        "Initial item placed in identitycounter table. Table description JSON:",
        JSON.stringify(data)
      );
    });
  }
});
