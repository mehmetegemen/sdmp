const Joi = require("joi");

module.exports = body => {
  const mainSchema = Joi.object({
    address: Joi.string().required(),
    company: Joi.string().required(),
    title: Joi.string().required(),
    interest: Joi.array(),
    name: Joi.string().required(),
    email: Joi.string().required(),
    mobile_number: Joi.number().required()
  });
  const { error } = Joi.validate(body, mainSchema, { convert: false });
  return error;
};
