const Joi = require("joi");

module.exports = body => {
  const mainSchema = Joi.object({
    address: Joi.string(),
    company: Joi.string(),
    title: Joi.string(),
    interest: Joi.array(),
    name: Joi.string(),
    email: Joi.string(),
    mobile_number: Joi.number()
  });
  const { error } = Joi.validate(body, mainSchema, { convert: false });
  return error;
};
