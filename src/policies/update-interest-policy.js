const Joi = require("joi");

module.exports = body => {
  const mainSchema = Joi.object({
    interest: Joi.array()
      .min(1)
      .required(),
    remove: Joi.boolean().required()
  });
  const { error } = Joi.validate(body, mainSchema, { convert: false });
  return error;
};
