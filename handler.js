const dbType = process.env.DBTYPE || "mongodb";
const createProfile = require("./src/handlers/create-profile-" + dbType);
const updateProfile = require("./src/handlers/update-profile-" + dbType);
const updateInterest = require("./src/handlers/update-interest-" + dbType);
const get = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;

  callback(null, {
    statusCode: 200,
    body: JSON.stringify({ Hello: "Hello" })
  });
};

module.exports = {
  createProfile,
  updateProfile,
  updateInterest,
  get
};
