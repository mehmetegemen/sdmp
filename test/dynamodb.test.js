const createUserLambda = require("../src/handlers/create-profile-dynamodb");
const updateUserLambda = require("../src/handlers/update-profile-dynamodb");
const updateInterestLambda = require("../src/handlers/update-interest-dynamodb");
const removeUser = require("../src/lib/remove-user-dynamodb");
const findUser = require("../src/lib/find-user-dynamodb");

beforeAll(async () => {
  // Runs before all tests in this block
  const criteria = {
    key: "email",
    value: "test@test.com"
  };
  // Look for a test user
  const userResponse = await findUser(criteria);
  if (userResponse.Count > 0)
    // A user is found
    // Remove it
    return removeUser(userResponse.Items[0].user_id);
});

test("should create profile", async () => {
  const body = {
    name: "test",
    address: "test",
    company: "test",
    email: "test@test.com",
    mobile_number: 555002,
    interest: ["intTest"],
    title: "test"
  };
  const event = {
    body: JSON.stringify(body)
  };
  const context = {};

  const data = await createUserLambda(event, context);
  //console.log(data.body);
  const returnedBody = JSON.parse(data.body);
  expect(returnedBody.name).toBe(body.name);
  expect(returnedBody.address).toBe(body.address);
  expect(returnedBody.company).toBe(body.company);
  expect(returnedBody.email).toBe(body.email);
  expect(returnedBody.mobile_number).toBe(body.mobile_number);
  expect(returnedBody.interest).toEqual(body.interest);
  expect(returnedBody.title).toBe(body.title);
});

test("should update profile", async () => {
  const body = {
    name: "testChanged",
    address: "testChanged",
    company: "testChanged",
    email: "testChanged@test.com",
    mobile_number: 444002,
    interest: ["testChanged"],
    title: "testChanged"
  };
  const event = {
    body: JSON.stringify(body),
    queryStringParameters: { id: "test@test.com" }
  };
  const context = {};

  const data = await updateUserLambda(event, context);
  //console.log(data.body);
  const returnedBody = JSON.parse(data.body);
  expect(returnedBody.name).toBe(body.name);
  expect(returnedBody.address).toBe(body.address);
  expect(returnedBody.company).toBe(body.company);
  expect(returnedBody.email).toBe(body.email);
  expect(returnedBody.mobile_number).toBe(body.mobile_number);
  expect(returnedBody.interest).toEqual(body.interest);
  expect(returnedBody.title).toBe(body.title);
  //return data;
});

test("should update interests by not removing them", async () => {
  const body = {
    remove: false,
    interest: ["testChanged", "testAdded"]
  };
  const event = {
    body: JSON.stringify(body),
    queryStringParameters: { id: "testChanged@test.com" }
  };
  const context = {};

  const data = await updateInterestLambda(event, context);
  console.log(data.body);
  const returnedBody = JSON.parse(data.body);
  // body has uppercase letters but
  // update-interest changes them into
  // lowercase letters.
  expect(returnedBody.interest).not.toEqual(body.interest);
  // Now check lowercase versions
  for (const i of body.interest.keys()){
    body.interest[i] = body.interest[i].toLowerCase();
  }
  for (const i of returnedBody.interest.keys()){
    returnedBody.interest[i] = returnedBody.interest[i].toLowerCase();
  }
  // Now expect them to be equal
  expect(returnedBody.interest).toEqual(body.interest);
});

test("should update interests by removing them", async () => {
  const body = {
    remove: true,
    interest: ["testChanged", "testAdded"]
  };
  const event = {
    body: JSON.stringify(body),
    queryStringParameters: { id: "testChanged@test.com" }
  };
  const context = {};

  const data = await updateInterestLambda(event, context);
  console.log(data.body);
  const returnedBody = JSON.parse(data.body);
  for (const i of returnedBody.interest.keys()){
    returnedBody.interest[i] = returnedBody.interest[i].toLowerCase();
  }
  // Now expect them to be equal
  expect(returnedBody.interest).toEqual([]);
});

afterAll(async () => {
  // Runs before all tests in this block
  const criteria = {
    key: "email",
    value: "testChanged@test.com"
  };
  // Look for a test user
  const userResponse = await findUser(criteria);
  if (userResponse.Count > 0)
    // A user is found
    // Remove it
    return removeUser(userResponse.Items[0].user_id);
});
